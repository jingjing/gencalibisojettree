#include "../../residualcalibration/macros/CalibTools.h"
#include "../../residualcalibration/macros/ResidualTools.h"
#include "../../residualcalibration/Correction3D/CorrelatedResidual.h"
#include <string>
#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TList.h"
#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TSystem.h"
#include "TColor.h"
#include "TChain.h"
#include "TLegend.h"
#include "TF2.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TH3F.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TEnv.h"


int FindBinInArray(int nBins, float value){
  //  float EtaBins[] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.8,4.0,4.2,4.5,4.9};
  float EtaBins[] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.7,2.9,3.0,3.075,3.15,3.25,3.35,3.45,3.6,3.8,4.1,4.5,4.9};
  int bin =0;
  for(int iBin=0; iBin<nBins; ++iBin){
    if(fabs(value)>=EtaBins[iBin] && fabs(value)<EtaBins[iBin+1]){
      bin = iBin;
    }
  }
  return bin;
}

void GenCalibratedNtuples(std::string input, std::string paramsFile, std::string treeName, std::string outName, std::string inputConfig, bool Apply1D=false){

  //Reading input
  TString inputFileString(input);
  TFile* inputFile = TFile::Open(inputFileString);
  //  TTree* JetTree = (TTree*)inputFile->Get(treeName);
  TString treeNameS(treeName);
  TTree* JetTree = (TTree*)inputFile->Get(treeNameS);

  //Reading corr. params
  TFile* params = NULL;
  TList* l_3Dparams = NULL;
  TList* l_DeltaPt = NULL;

  if(paramsFile!="NONE"){
    TString paramsString(paramsFile);
    params = TFile::Open(paramsString);
    l_3Dparams = (TList*) params->Get("parameters");
    std::cout<<"3D corr. parameters"<<std::endl;
    l_DeltaPt = (TList*) params->Get("dPtParams");
    std::cout<<"Delta pT parameters"<<std::endl;
  }

  int nEvents = JetTree->GetEntries();
  std::cout<<"Looping over "<<nEvents<<" in "<<treeName<<std::endl;

  //Output Tree
  TString outNameS(outName);
  TFile outFile(outNameS,"RECREATE");
  TTree* NewTree = (TTree*)JetTree->CloneTree(0);
  NewTree->Print();
  NewTree->SetBranchStatus("*",1);
//  NewTree->SetBranchStatus("jet_ConstitE",0);

  //Rsidual pT branch
  std::vector<float> pT_res;
  std::vector<float> E_res;
  std::vector<float> pT_3D;
  std::vector<float> E_3D;
  std::vector<float> pT_4;
  std::vector<float> E_4;
  std::vector<float> pT_1D;
  std::vector<float> E_1D;
  NewTree->Branch("pT_res",&pT_res);
  NewTree->Branch("E_res",&E_res);
  NewTree->Branch("pT_3D",&pT_3D);
  NewTree->Branch("E_3D",&E_3D);
  NewTree->Branch("pT_4",&pT_4);
  NewTree->Branch("E_4",&E_4);
  NewTree->Branch("pT_1D",&pT_1D);
  NewTree->Branch("E_1D",&E_1D);
  // Open config
  TString configName (inputConfig);
  TEnv* config = new TEnv(configName);
  if(!config){
    std::cout << configName << " not found, exiting" << std::endl;
    //return 0;
  }
  config->Print();

  //some variables needed for the 1D correction
  std::vector<double> etaFitBins;
  etaFitBins = VectorizeD(config->GetValue("ResidualAbsEtaBins","0 0.9 1.2 1.5 1.8 2.4 2.8 3.2 3.5 4.0 4.3 6.0"));
  std::cout<<"etaFitBins Read"<<std::endl;
  std::vector<double> NPVTerm = VectorizeD(config->GetValue("NPVTerm","0.061 -0.005 0.082 -0.065 0.078 0.058 0.544 -0.310 -0.080 0.265 -0.005 0.120"));
  std::cout<<"NPVTerm Read"<<std::endl;
  std::vector<double> MuTerm = VectorizeD(config->GetValue("MuTerm","0.002 -0.006 -0.045 -0.011 -0.099 -0.077 -0.367 0.140 -0.031 -0.160 -0.009 -0.019"));
  std::cout<<"MuTerm Read"<<std::endl;
  std::vector<double> NPVoffset;
  std::vector<double> Muoffset;
  if(Apply1D){
    NPVoffset = computeoffsets(etaFitBins,NPVTerm);
    Muoffset = computeoffsets(etaFitBins,MuTerm);
  }

  //Defining some needed variables
  std::vector<float>* pt = NULL; //true pt
  std::vector<float>* recojet_pt = NULL; //const scale pt
  std::vector<float>* recoE = NULL; //const scale E
  Int_t           NPV = 0;
  Float_t         mu = 0;
  std::vector<float>*  jet_area = NULL; //area
  Double_t        rho = 0;
  std::vector<float>* true_eta = NULL;
  //  Float_t         weight=0;
  Double_t         weight=0;
  Float_t         weight_xs=0;
  std::vector<float>* recojet_eta = NULL;
  Int_t bcid = 0;

  double AreaCorr = 0;
  double calibration3D = 0;
  double EstimatedPtTrue = 0;
  double DeltaPt = 0;
  int EtaBin = 0;
  int nEtaBins = 41;
  double calibRatio;
  double calib3DRatio;
  double calib4Ratio;
  double calib4;
  double NPVtermAtEta; // npv term for 1d correction
  double MutermAtEta; // mu term for 1d correction
  double corr1D = 0.0;
  double calib1Dratio;
  std::vector <TString> branches;
  branches = ReadBranches(config);

  JetTree->SetBranchAddress(branches[0], &pt);
  JetTree->SetBranchAddress(branches[1], &recojet_pt);
  JetTree->SetBranchAddress(branches[2], &NPV);
  JetTree->SetBranchAddress(branches[3], &mu);
  JetTree->SetBranchAddress(branches[4], &rho);
  JetTree->SetBranchAddress(branches[5], &jet_area);
  JetTree->SetBranchAddress(branches[6], &true_eta);
  JetTree->SetBranchAddress(branches[7], &weight);
  JetTree->SetBranchAddress(branches[8], &weight_xs); //not needed, weigth should include all weights applied
  JetTree->SetBranchAddress(branches[9], &recojet_eta);
  JetTree->SetBranchAddress(branches[10], &bcid);
  JetTree->SetBranchAddress("jet_ConstitE", &recoE);

  std::vector< std::vector<int> > m_closestNonEmpty; //Needed for Get3Dcorrection but not really used now

  //List of variables for P-A
  //std::ofstream corrExamples;
  //corrExamples.open("corrExamples.txt");
  //corrExamples<<"pt ,     eta ,     area,       rho,     mu,   NPV, corrected pt, etabin, calibration3D,   DeltaPt,   pTtrueEst"<<std::endl;

  //Loop on event
  for(int ievt=0; ievt<nEvents;++ievt){
    JetTree->GetEntry(ievt);
    if (ievt%1000==0) std::cout<<"Entry: "<<ievt<<std::endl;
    for(int j=0; j<pt->size(); ++j){
      //Area Correction
      AreaCorr = jet_area->at(j)*rho*0.001;
      //3D correction
      EtaBin = FindBinInArray(nEtaBins, recojet_eta->at(j));
      //      std::cout<<"EtaBin"<<EtaBin<<std::endl;
      if(paramsFile!="NONE"){
        if(recojet_pt->at(j)-AreaCorr < 0) calibration3D = Get3DCorrection(fabs(recojet_eta->at(j)), EtaBin, 1.0, mu, NPV,l_3Dparams, 2,-1,m_closestNonEmpty); //when ptarea is negative evaluating at 1MeV
        else calibration3D = Get3DCorrection(fabs(recojet_eta->at(j)), EtaBin, recojet_pt->at(j)-AreaCorr, mu, NPV,l_3Dparams, 2,-1,m_closestNonEmpty);
      //if (calibration3D==0){
      //  std::cout<<"calibration3D = "<<calibration3D<<std::endl;
      //  std::cout<<"calibration3D = "<<calibration3D<<std::endl;
      //}
      //Delta Pt
        EstimatedPtTrue = recojet_pt->at(j) - AreaCorr - calibration3D;
        if (calibration3D && calibration3D!=-999.9 && calibration3D!=0) DeltaPt = GetdPtMean(EstimatedPtTrue,l_DeltaPt,fabs(recojet_eta->at(j)));
        else DeltaPt = 0;
      }
      else{
        calibration3D = 0;
        DeltaPt = 0;
      }
      //1D correction
      if(Apply1D){
        NPVtermAtEta = interpolation(etaFitBins,NPVTerm,NPVoffset,abs(true_eta->at(j)));
        std::cout<<"NPVtermAtEta = "<<NPVtermAtEta<<std::endl;
        //      std::cout<<"NPV term:"<<NPVtermAtEta<<std::endl;
        MutermAtEta = interpolation(etaFitBins,MuTerm,Muoffset,abs(true_eta->at(j)));
        std::cout<<"MutermAtEta = "<<MutermAtEta<<std::endl;
        //      std::cout<<"mu term:"<<MutermAtEta<<std::endl;
        corr1D = NPVtermAtEta*(NPV-1)+MutermAtEta*mu;
        std::cout<<"corr1D = "<<corr1D<<std::endl;

        //residual calibration option 1D
        pT_1D.push_back(recojet_pt->at(j) - AreaCorr -corr1D);
        std::cout<<"pT_1D = "<<(recojet_pt->at(j) - AreaCorr -corr1D)<<std::endl;

        calib1Dratio=(recojet_pt->at(j) - AreaCorr -corr1D)/recojet_pt->at(j);
        E_1D.push_back(recoE->at(j)*calib1Dratio);
      }

      //residual option 0
      pT_res.push_back(recojet_pt->at(j) - AreaCorr - calibration3D + DeltaPt);
      calibRatio=(recojet_pt->at(j) - AreaCorr - calibration3D + DeltaPt)/recojet_pt->at(j);
      E_res.push_back(recoE->at(j)*calibRatio);
      //E_res.push_back((recojet_pt->at(j) - AreaCorr - calibration3D + DeltaPt)*cosh(recojet_eta->at(j)));
      //residual option 1
      pT_3D.push_back(recojet_pt->at(j) - AreaCorr - calibration3D);
      calib3DRatio=(recojet_pt->at(j) - AreaCorr - calibration3D)/recojet_pt->at(j);
      E_3D.push_back(recoE->at(j)*calib3DRatio);

      //residual calibration option 4
      if(paramsFile!="NONE") calib4 = Get3DCorrection(fabs(recojet_eta->at(j)), EtaBin, recojet_pt->at(j)-AreaCorr, 33.7, 20,l_3Dparams, 2,-1,m_closestNonEmpty);
      else calib4=0;
      pT_4.push_back(recojet_pt->at(j) - AreaCorr - calibration3D + calib4);
      calib4Ratio=(recojet_pt->at(j) - AreaCorr - calibration3D + calib4)/recojet_pt->at(j);
      E_4.push_back(recoE->at(j)*calib4Ratio);
      double corrPt = recojet_pt->at(j) - AreaCorr - calibration3D + DeltaPt;

      

      //if(!isnan(corrPt) && corrPt != 0){
      //  corrExamples<<" "<<recojet_pt->at(j)<<"  "<<recojet_eta->at(j)<<"  "<<jet_area->at(j)<<"  "<<rho<<"  "<<"  "<<mu<<"  "<<NPV<<"  "<<corrPt<<"  "<<EtaBin<<"  "<<calibration3D<<"  "<<DeltaPt<<"  "<<EstimatedPtTrue<<std::endl;
      //}
      //      std::cout<<"reco pT = " <<recojet_pt->at(j)<<"  eta = "<<fabs(recojet_eta->at(j))<<"  3D correction = "<<calibration3D<< "  Delta pT = "<<DeltaPt<<std::endl;
      AreaCorr = 0;
      calibration3D = 0;
      DeltaPt = 0;

    } //Close loop on jets
    //std::cout<<"jets"<<std::endl;
    NewTree->Fill();
    pT_res.clear();
    E_res.clear();
    pT_3D.clear();
    E_3D.clear();
    pT_4.clear();
    E_4.clear();
    pT_1D.clear();
    E_1D.clear();
    //if(ievt==1000000){break;}
  } //Close loop in entries

  outFile.cd();
  //  NewTree->Write();
  outFile.Write();
  outFile.Close();


} //Close GenCalibNtuples


int main(int argc ,char* argv[]){

  //temporary to solve out name problem

  std::string path = "/nfs/dust/atlas/user/rivadene/qualification/IsoJetTrees_inputs/NoPtCut/JZWithSW_230819/";
  //Read the arguments
  std::string input = argv[1];
  std::cout<<"input = "<<input<<std::endl;
  std::string paramsFile = argv[2];
  std::cout<<"paramsFile = "<<paramsFile<<std::endl;
  std::string TreeName = "IsolatedJet_tree";
  std::string outFileName = "calibrated_"+input;
  //  std::string outFileName = "calibrated.root";
  std::string inputConfig = argv[3];
  std::cout<<"config = "<<inputConfig<<std::endl;
  bool Apply1D = argv[4];
  std::cout<<"Apply1D? "<<Apply1D<<std::endl;

  //Adding path to input file
  input = path + input;

  //Call the function
  GenCalibratedNtuples(input,paramsFile,TreeName,outFileName,inputConfig,Apply1D);


}

//bash-4.1$ g++ -o GenCalibNtuples GenCalibratedNtuples.cxx `root-config --cflags --glibs`
